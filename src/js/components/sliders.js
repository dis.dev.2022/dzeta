/*
Документация по работе в шаблоне:
Документация слайдера: https://swiperjs.com/
Сниппет(HTML): swiper
*/

import Swiper, {Autoplay, Navigation, Pagination, Thumbs} from 'swiper';
Swiper.use([Navigation, Pagination, Autoplay, Thumbs]);

// Инициализация слайдеров
function initSliders() {
    // Перечень слайдеров
    if (document.querySelector('[data-influencer]')) {
        new Swiper('[data-influencer]', {
            modules: [Navigation],
            observer: true,
            loop: true,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 30,
            speed: 600,
            navigation: {
                nextEl: '[data-influencer-next]',
                prevEl: '[data-influencer-prev]',
            }
        });
    }

    if (document.querySelector('[data-mystery-gallery]')) {
        new Swiper('[data-mystery-gallery]', {
            modules: [Autoplay],
            observer: true,
            loop: true,
            freeMode: {
                enabled: true,
                sticky: true,
            },
            loopedSlides: 5,
            observeParents: true,
            slidesPerView: "auto",
            spaceBetween: 0,
            speed: 4000,
            autoplay: {
                delay: 1,
             //   waitForTransition: false,
                disableOnInteraction: false,
            },
            breakpoints: {
                768: {
                    direction: "vertical",
                    autoplay: {
                        delay: 1,
                        //   waitForTransition: false,
                        disableOnInteraction: false,
                    },
                }
            },
        });
    }


    if (document.querySelector('[data-team]')) {

        let teamPhotos = new Swiper('[data-team-photos]', {
            modules: [Navigation],
            observer: true,
            loop: false,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 150,
            breakpoints: {
                "768": {
                    spaceBetween: 250,
                },
                "992": {
                    spaceBetween: 400,
                },
                "1200": {
                    spaceBetween: 40,
                },
                "1400": {
                    spaceBetween: 140,
                },
            },
        });

        let teamContent = new Swiper('[data-team-content]', {
            modules: [Navigation],
            observer: true,
            loop: false,
            simulateTouch: false,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 150,
            breakpoints: {
                "768": {
                    spaceBetween: 250,
                },
                "992": {
                    spaceBetween: 400,
                },
                "1200": {
                    spaceBetween: 50,
                },
                "1400": {
                    spaceBetween: 140,
                },
            },
            navigation: {
                nextEl: '[data-team-next]',
                prevEl: '[data-team-prev]',
            },
            on: {
                slideNextTransitionStart: function () {
                    teamPhotos.slideNext(320);
                },
                slidePrevTransitionStart: function () {
                    teamPhotos.slidePrev(320);
                },
            }
        });
    }
}

window.addEventListener("load", function (e) {
    // Запуск инициализации слайдеров
    initSliders();
});
