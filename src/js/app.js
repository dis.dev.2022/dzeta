"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import mobileNav from './components/mobile-nav.js';  // Мобильное меню
import {WebpMachine} from "webp-hero"
import HystModal from 'hystmodal';

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// IE Webp Support
const webpMachine = new WebpMachine();
webpMachine.polyfillDocument();

// mobile 100vh
usefulFunctions.fullVHfix();

// Меню
mobileNav();

// Modal
const myModal = new HystModal({
    linkAttributeName: 'data-hystmodal',
});


// Sliders
import "./components/sliders.js";


// items navigation
(() => {

    $('[data-item]')
        .mouseenter(function() {
            if (document.documentElement.clientWidth > 1199) {
                let elem = $(this).find('.item__hover');
                $(this).addClass('hover');
                elem.slideDown(300);
            }
        })
        .mouseleave(function(){
            if (document.documentElement.clientWidth > 1199) {
                let elem = $(this).find('.item__hover');
                $(this).removeClass('hover');
                elem.slideUp(300);
            }
        });

    window.addEventListener('resize', function(event) {
        $('.item__hover').removeAttr('style');
    });
})();


(() => {
    if (document.querySelector('[data-form]')) {

        document.addEventListener('click', formSwitcher);

        function formSwitcher (event) {

            if (event.target.closest('[data-switch-login]')) {
                document.querySelector('[data-form-switcher]').classList.toggle('form-switch--auth');
                $('[data-form-reg]').slideUp(300);
                $('[data-form-forgot]').slideUp(300);
                $('[data-form-password]').slideDown(300);
                $('[data-form-auth]').slideDown(300);
            }

            if (event.target.closest('[data-switch-register]')) {
                document.querySelector('[data-form-switcher]').classList.toggle('form-switch--auth');
                $('[data-form-forgot]').slideUp(300);
                $('[data-form-auth]').slideUp(300);
                $('[data-form-password]').slideDown(300);
                $('[data-form-reg]').slideDown(300);
            }

            if (event.target.closest('[data-form-pwd]')) {
                document.querySelector('[data-form]').classList.toggle('form-block--forgot');
                $('[data-form-auth]').slideUp(300);
                $('[data-form-reg]').slideUp(300);
                $('[data-form-password]').slideUp(300);
                $('[data-form-forgot]').slideDown(300);
            }
        }
    }
})();

window.addEventListener('click', function(event) {
    if (event.target.closest('[data-float-toggle]')) {
        document.querySelector('[data-float]').classList.toggle('open');
    }
});

// Checkout
(() => {
    window.addEventListener('click', function(event) {
        if (event.target.closest('[data-checkout]')) {
            document.querySelector('[data-mystery]').classList.remove('successfully');
            myModal.open('#buy');
        }
        if (event.target.closest('[data-checkout-submit]')) {
            myModal.close();
            setTimeout(() => {
                document.querySelector('[data-mystery]').classList.add('successfully');

            }, 500);

        }
    });
})();






