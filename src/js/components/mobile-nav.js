
export default () => {
    const self = document.querySelector('body');
    const pageNavToggle = document.querySelectorAll('[data-nav-toggle]');

    if (pageNavToggle) {
        pageNavToggle.forEach(el => {
            el.addEventListener('click', (e) => {
                self.classList.toggle('nav-open');
                return false;
            });
        });
    }

    (() => {
        $('[data-nav-item]')
            .mouseenter(function(){
                let $this = $(this);
                let path = $this.attr('data-nav-item');
                let target = `[data-nav-target="${path}"]`
                $('[data-nav-target]').removeClass('active');
                $(target).addClass('active');
            })
            .mouseleave(function(){
                $('[data-nav-target]').removeClass('active');
            })
    })();

};
